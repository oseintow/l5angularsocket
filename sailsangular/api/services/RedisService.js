var redis = require("redis");
var sub = redis.createClient();
var pub = redis.createClient();
var msg_count = 0;

sub.on("subscribe", function (channel, count) {
    pub.publish("a nice channel", "I am sending a message.");
    pub.publish("a nice channel", "I am sending a second message.");
    pub.publish("a nice channel", "I am sending my last message.");
});

sub.on("message", function (channel, message) {
    console.log("sub channel " + channel + ": " + message);
    msg_count += 1;
    if (msg_count === 3) {
        //sub.unsubscribe();
        //sub.quit();
        //pub.quit();
    }

    if( channel == "logedin")
        logedin(message);
});

//sub.subscribe("a nice channel");
sub.subscribe("test-channel");
sub.subscribe("logedin");
//sub.subscribe("pmessage");

function logedin(msg){
    console.log("hello");
}