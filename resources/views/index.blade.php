<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
	<meta charset="UTF-8">
	<title>Laravel Angular Socket</title>
</head>
<body>

<div ng-controller="HomeController">
	<p ng-bind="app"></p>

	<div>
		<div ng-bind="msg"></div>

		<div style="min-width: 100%">

			<div style="float:left; min-width: 200px; min-height: 300px;margin:20px; border: solid red 1px">
				<ul>
					<li ng-repeat="msg in messages.details track by $index" ng-bind="msg"></li>
				</ul>
			</div>

			<div style="float:left">
				<ul>
					<li ng-repeat="user in usernames.details track by $index" ng-bind="user"></li>
				</ul>
			</div>
			
		</div>
		

		<div style="clear:both">
			<form name="frm" novalidate>
				<input type="text" name="chat" ng-model="chat">
				<input type="submit" value="message" ng-click="sendMessage()">
			</form>

		</div>


		<hr>

		<div class="container">
			<form method="post">
				<label for="">Username:</label>
				<input type="text" name="username" ng-model="username">
				<input type="text" name="password" ng-model="password">
				<input type="submit" ng-click="authenticate()">
			</form>
		</div>


	</div>
</div>

<script src="assets/js/angular.js"></script>
<script src="assets/js/socket.js"></script>
<script type="text/javascript">

var app = angular.module('app',[]);

app.factory('socket', function ($rootScope, $window) {
  // var socket = io.connect();
  var socket = io($window.location.hostname + ':3600');
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {  
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});

app.controller('HomeController',function($scope,socket,$http,$window){
		$scope.messages = {
			details:[]
		};
		$scope.app = "Chat Application";

		$scope.usernames = {
			details: []
		};

		$scope.chat = "";

        //var socket = io('http://192.168.10.10:3000');
        socket.on("users:App\\Events\\AddUsersToSocket", function(data){
			console.log(data.user);
            $scope.users.details.push(data.user);
        });	

        socket.on('messages', function(data, callback) {
			$scope.messages.details.push(data.msg);
		});

		// $scope.chat = "";
        $scope.sendMessage = function(){

			socket.emit("chat message",{},function(data){
				console.log("php socket");
			});

        	if ($scope.chat == "") return;

        	socket.emit('send message', $scope.chat, function(data){
				if(data){
					$scope.chat = "";
					$scope.messages.details.push(data);
				} else{

				}
			});
        }

        $scope.authenticate = function(){
        	// alert("http://" + $window.location.hostname);
        	$http.post("http://" + $window.location.hostname + ":5000/login",{username: $scope.username, password: $scope.password})
        	.success(function(data){
        		socket.emit('new user', data.user.username, function(data){
					if(data){
					} else{
        				alert("username taken");
					}
				});
        	}).
        	error(function(data){
        		alert(data);
        	});
        }

        socket.on('usernames',function(data,callback){
        	if(data){
        		$scope.usernames.details.push(data);
        	}
        });

		socket.on('logedin:newvisitor',function(data,callback){
			console.log(data);
		});

        socket.on("get all messages",function(data,callback){
        	if(data){
        		angular.foreach(data,function(value){
        			$scope.messages.details.push(value);
        		})
        	}
        });

        $scope.getMessages = function(){

        	socket.emit("get all messages",{},function(data){
	        	if(data){
	        		msg = JSON.parse(data.msg);
	        		angular.forEach(msg,function(value){
	        			$scope.messages.details.push(value);
	        		});

	        		angular.forEach(data.users,function(value){
	        			$scope.usernames.details.push(value);
	        		});
	        	}
	        });
        }
        
        $scope.getMessages(); 

});

</script>

</body>
</html>