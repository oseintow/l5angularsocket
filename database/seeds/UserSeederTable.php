<?php

use Illuminate\Database\Seeder;

class UserSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
        	'username' => "michael",
        	'password' => bcrypt('password')
        ]);
        
        \App\User::create([
        	'username' => "diana",
        	'password' => bcrypt('password')
        ]);

        \App\User::create([
        	'username' => "esther",
        	'password' => bcrypt('password')
        ]);

        \App\User::create([
        	'username' => "mon",
        	'password' => bcrypt('password')
        ]);

        \App\User::create([
        	'username' => "sammy",
        	'password' => bcrypt('password')
        ]);
    }
}
