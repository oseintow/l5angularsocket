var express = require('express')(handler);
var app = require('http').Server(express);
var io = require('socket.io')(app);

// var app = require('http').createServer(handler);
// var io = require('socket.io')(app);

var Redis = require('ioredis');
var redis = new Redis();

var users = {};
var messages = [];

// var mysql      = require('mysql');
// var connection = mysql.createConnection({
//   host     : 'localhost',
//   user     : '< MySQL username >',
//   password : '< MySQL password >',
//   database : '<your database name>'
// });

// connection.connect();
// 
// app.use(function (req, res, next) {
//    	res.writeHead(200);
//     res.end('');
//   	next();
// });

app.listen(3600, function() {
    console.log('Server is running!');
});

function handler(req, res) {
    res.writeHead(200);
    res.end('');
}

// connection.query('SELECT * from < table name >', function(err, rows, fields) {
//   if (!err)
//     console.log('The solution is: ', rows);
//   else
//     console.log('Error while performing Query.');
// });

// connection.end();

io.on('connection', function(socket) {
	//console.log(socket);
	socket.on('new user', function(data,callback){
		//console.log('Message Recieved: ' + data);

		if (data in users){
			callback(false);
		} else{
			callback(true);
			socket.nickname = data;
			users[socket.nickname] = socket;
			updateUsers(data);
		}
	});

	socket.on("get all messages",function(data,callback){
		//console.log(messages);
		callback({msg:JSON.stringify(messages), users: Object.keys(users)});
	});

	function updateUsers(data){
		io.emit('usernames', data);
		console.log('Emitting message: ' + data);
		// io.emit('usernames', data);
	}

	function getAllMessages(){
		io.emit("get all messages",messages)
	}

    socket.on('send message', function(data, callback) {
		console.log('Message Recieved: ' + data);
		messages.push(data);
    	// message = JSON.parse(message);
    	// io.emit("messages", {"msg":data});

    	var getIndex  = data.indexOf(" ");
    	if(getIndex !== -1){
    		user = data.substr(0,getIndex)
    		console.log(user);
    		if(user in users){
    			body = data.substr(getIndex + 1);
    			users[user].emit("messages",{msg: body});
    			callback(body);
    		}else{
    			// callback({"msg":data})
    			io.emit("messages", {"msg":data});
    		}
    	}else{
    		// callback({"msg":data})
    		io.emit("messages", {"msg":data});
    	}
	});

	// socket.on('disconnect',function(data){
	// });
});

redis.psubscribe('*', function(err, count) {
     console.log("in redis pub")
});

//
// redis.subscribe('users', function(err, count) {
// 	console.log(err);
// 	console.log(count);
// });
// 

// redis.subscribe('logedin', function(err, count) {
// 	console.log(err);
// 	console.log(count);
// });


redis.on('pmessage', function(subscribed, channel, message) {
	console.log('Message Recieved: ' + message);
	console.log('Message Channel: ' + channel);
    message = JSON.parse(message);

    if(message.event == "App\\Events\\AddUsersToSocket"){
    	console.log("Am in Add users to socket");
    	// michael.emit("whisper" + ":" + message.event, message.data);
    }

    io.emit(channel + ':' + message.event, message.data);
});