<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// use \App\Http\Requests\Request;
use App\Http\Requests\UserRequest;
use App\Events\AddUsersToSocket;

use Illuminate\Support\Facades\Redis;


Route::get('/', function (){
	$user = \App\User::findOrFail(1);
	event(new \App\Events\AddUsersToSocket($user));

    return view('index');
});

get('login',function(){
	return view('login');
});

Route::post('login',function(UserRequest $request){
	if (\Auth::attempt(["username"=>$request->username,"password"=>$request->password])){
		event(new AddUsersToSocket(\Auth::user()));
		// return "Added";
		return response()->json(["user"=>\Auth::user()],200);
		// return redirect("/");
	}else{
		return response()->json(["error"=>"Error loggin in"],403);
	}
});

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
get('queue',function(Request $request){

	$user = \App\User::findOrFail(1);

	event(new \App\Events\AddUsersToSocket($user));
//    event(new \App\Jobs\QueueJob($user));
	return "event fired";
});

get("job","QueueJob@index");



