<?php

// composer autoload
//include __DIR__ . '/../vendor/autoload.php';
//include __DIR__ . '/../src/autoload.php';

include "index.php";

use Workerman\Worker;
use Workerman\WebServer;
use PHPSocketIO\SocketIO;
//use Log;

//\Log::info("box 1");

$io = new SocketIO(3600);
$io->on('connection', function($socket)use($io){
    $socket->on('chat message', function($msg)use($io){
        echo "michael";
//        \Log::info("tom");
        $io->emit('chat message', $msg);
    });
});

$web = new WebServer('http://0.0.0.0:2022');
$web->addRoot('localhost', __DIR__ . '/public');
Worker::runAll();
