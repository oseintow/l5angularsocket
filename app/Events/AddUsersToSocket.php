<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\User;
use Illuminate\Support\Facades\Redis;
//use Illuminate\Redis\Database as Redis;

class AddUsersToSocket extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $user;
    public $username;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
//        $redis = Redis::connection();
////        $redis->connection();
//        $redis->set('tutorials', "mike");
//        $redis->subscribe(['*'], function($message, $channel) {
//        //	\Log::info($message);
//        });
        $this->user     = $user;
        $this->username = $user->username;
        Redis::publish('test-channel', json_encode(['foo' => 'bar']));
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ["logedin"];
    }

    public function broadcastAs()
    {
        return 'newvisitor';
    }
}
