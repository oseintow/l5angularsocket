<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         \Queue::failing(function ($connection, $job, $data) {
            // Notify team of failing job...
            \Log::info("======= failed jobs =======");
            \Log::info($job);
            \Log::info($data);

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
